console.log(1 == 1);
//true

console.log("a" > "b");
//false

console.log("1" === 1);
//false
//so sanh gia tri va kieu
console.log("1" !== 1);

var average = 5;
if (average >= 5 && average <= 10) {
  console.log("len lop");
} else {
  console.log("o lai lop");
}

var n = 45;
var salary = 100;
var totalSalary = 0;

if (n <= 40) {
  totalSalary = n * salary;
  console.log(totalSalary);
} else {
  totalSalary = 40 * salary + (n - 40) * salary * 1.5;
  console.log(totalSalary);
}

var name = "tao";
var n = 110;
var price = 100;
var totalMoney;

if (n < 50) {
  totalMoney = n * price;
  console.log(totalMoney);
} else if (n >= 50 && n <= 100) {
  totalMoney = 50 * price + (n - 50) * price * (1 - 0.08);
  console.log(totalMoney);
} else {
  totalMoney =
    50 * price + (n - 50) * price * (1 - 0.08) + (n - 100) * price * 0.88;
  console.log(totalMoney);
}

var math = 4;
var hoa = 4;
var ly = 4;
var tb = (math + hoa + ly) / 3;

if (tb >= 8.5) {
  console.log("hoc sinh gioi");
} else if (tb >= 6.5) {
  console.log("hoc sinh kha");
} else if (tb >= 5) {
  console.log("hoc sinh trung binh");
} else {
  console.log("hoc sinh yeu");
}

//falsy: 0,"",null,undefined,false,NaN;
//truthy: con lai;

var n = 2;

switch (n) {
  case 1:
    console.log("so mot");
    break;
  default:
    console.log("ko doc dc");
}

var month = 1;

switch (month) {
  case 1:
    console.log("thang 1");
    break;
  case 2:
    console.log("thang 2");
    break;
  case 3:
    console.log("thang 3");
    break;
  default:
    console.log("ko hop le");
}
/**
 * if(a%2 ===0){
 * console.log("chan")}
 *
 * if(a%2===1{
 * console.log("le")}
 */
var a = 1;
var b = 2;
var c = 3;
var chan = 0;
var le = 0;

if (a % 2 === 0) {
  chan++;
} else {
  le++;
}
if (b % 2 === 0) {
  chan++;
} else {
  le++;
}
if (c % 2 === 0) {
  chan++;
} else {
  le++;
}

console.log("tong so chan: " + chan, "tong so le: " + le);

// bài tập 1
var soTien, tienThang, soDu, phat;
soTien = 900;
tienThang = 1000;
soDu = tienThang - soTien;
phat = soDu * (1.5 / 100);
if (soDu > 0) {
  console.log("tiền phạt", phat);
} else {
  console.log("ko có tiền phạt");
}

// bt1
var a, b, c;
a = 5;
b = 20;
c = 30;

if (a > b && a > c) {
  if (b > c) {
    console.log(a, b, c);
  } else {
    console.log(a, c, b);
  }
}

// bt2
var bo, me, anh, chi, may;
// bo = 1;
// me = 2;
// anh = 3;
// chi = 4;
may = 2;
switch (may) {
  case 1:
    console.log("chao bo");
    break;
  case 2:
    console.log("chao me");
    break;
}
