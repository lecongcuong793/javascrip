var a, b, c;
a = 50;
b = 20;
c = 10;

if (a > b && a > c) {
  if (b > c) {
    console.log(a, b, c);
  } else {
    console.log(a, c, b);
  }
} else if (b > a && b > c) {
  if (a > c) {
    console.log(b, a, c);
  } else {
    console.log(b, c, a);
  }
} else {
  if (a > b) {
    console.log(c, a, b);
  } else {
    console.log(c, b, a);
  }
}
